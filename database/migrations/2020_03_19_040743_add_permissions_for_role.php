<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class AddPermissionsForRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [
            [
                'name' => 'Create users',
                'slug' => 'create.users',
                'description' => 'able to create new user',
            ],
            [
                'name' => 'View users',
                'slug' => 'view.users',
                'description' => 'able to view users',
            ],
            [
                'name' => 'Filter users',
                'slug' => 'filter.users',
                'description' => 'able to filter users',
            ],
            [
                'name' => 'edit users',
                'slug' => 'edit.users',
                'description' => 'able to edit user',
            ],
            [
                'name' => 'delete users',
                'slug' => 'delete.users',
                'description' => 'able to delete user',
            ],
            [
                'name' => 'delete users',
                'slug' => 'delete.users',
                'description' => 'able to delete user',
            ],
            [
                'name' => 'manage roles',
                'slug' => 'manage.roles',
                'description' => 'able to manage roles',
            ],
            [
                'name' => 'manage permissions',
                'slug' => 'manage.permissions',
                'description' => 'able to manage permissions',
            ],

        ];
        $admin = Role::where('slug', User::ADMIN)->first();
        foreach ($permissions as $permission) {
            $model = Permission::where('slug', $permission['slug'])->first();
            if (!$model) {
                $model = Permission::create($permission);
            } else {
                $model->update($permission);
            }
            $admin->attachPermission($model);
        }


        //research1
        $permissions = [
            [
                'name' => 'Create users',
                'slug' => 'create.users',
                'description' => 'able to create new user',
            ],

        ];
        $research1 = Role::where('slug', User::Research1)->first();
        foreach ($permissions as $permission) {
            $model = Permission::where('slug', $permission['slug'])->first();
            if (!$model) {
                $model = Permission::create($permission);
            } else {
                $model->update($permission);
            }
            $research1->attachPermission($model);
        }


        //research2
        $permissions = [
            [
                'name' => 'View users',
                'slug' => 'view.users',
                'description' => 'able to view users',
            ],
            [
                'name' => 'Filter users',
                'slug' => 'filter.users',
                'description' => 'able to filter users',
            ],

        ];
        $research1 = Role::where('slug', User::Research1)->first();
        foreach ($permissions as $permission) {
            $model = Permission::where('slug', $permission['slug'])->first();
            if (!$model) {
                $model = Permission::create($permission);
            } else {
                $model->update($permission);
            }
            $research1->attachPermission($model);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
