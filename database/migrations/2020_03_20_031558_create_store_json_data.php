<?php

use App\Models\JsonTable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreJsonData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $url = 'https://hashtagmails.com/pullData/index2.json';
        $json = json_decode(file_get_contents($url), true);
        foreach ($json as $j){
            JsonTable::create([
                'data'=>json_encode($j),
                'jsonObject'=>json_encode($j),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
