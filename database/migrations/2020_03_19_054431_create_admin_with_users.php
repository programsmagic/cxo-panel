<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use jeremykenedy\LaravelRoles\Models\Role;

class CreateAdminWithUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $admin = User::create([
            'name'=>'Super Admin',
             'email'=>'super-admin@email.com',
             'password'=>Hash::make('password'),
         ]);
        $role = Role::where('slug', '=', User::ADMIN)->first();
        $admin->attachRole($role);


        $research1 = User::create([
             'name'=>'Research1',
             'email'=>'research1@email.com',
             'password'=>Hash::make('password'),
         ]);
        $role = Role::where('slug', '=', User::Research1)->first();
        $research1->attachRole($role);

        $research2 = User::create([
            'name'=>'Research2',
            'email'=>'research2@email.com',
            'password'=>Hash::make('password'),
         ]);
        $role = Role::where('slug', '=', User::Research2)->first();
        $research2->attachRole($role);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_with_users');
    }
}
