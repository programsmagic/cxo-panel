<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//middleware for : Only Authorized User can access
Route::middleware(['auth:api'])->group(function () {

    Route::resource(  'users' ,'Api\UserController');
    Route::resource(  'roles' ,'Api\RoleController');
    Route::resource(  'users' ,'Api\UserController');
    Route::apiResources([
        'permissions' => 'Api\PermissionController',
    ]);



});
Route::apiResources([
    'json' => 'Api\JsonTableController',
]);
