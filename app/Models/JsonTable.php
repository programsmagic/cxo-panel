<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JsonTable extends Model
{
    protected $guarded = [];
}
