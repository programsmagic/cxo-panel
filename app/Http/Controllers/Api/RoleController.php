<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        return Role::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'slug' => 'required|string|max:191',
            'description' => 'nullable|string|max:191',
        ]);
        if (!$user->hasRole('admin')) {
            throw new AuthorizationException("Access Violation: Trying to do with not permitted Account");
        }
        $role=  Role::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
            'level' => 3,
        ]);
        $perm = $request->permissions;
        for($i=0;$i<count($request->permissions);$i++){
             $permission = Permission::find($perm[$i]);
             $role->attachPermission($permission);
        }

        return $role;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Role::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= Auth::user();
        if (!$user->hasRole('admin')) {
            throw new AuthorizationException("Access Violation: Trying to do with not permitted Account");
        }
        $user = Role::findOrFail($id);
        $user->delete();
        return response()->json(['message'=>'Role Deleted'],200);
    }
}
