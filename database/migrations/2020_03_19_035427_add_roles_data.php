<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use jeremykenedy\LaravelRoles\Models\Role;

class AddRolesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //'level' => 1, --> for super admin
        //'level' => 2, --> for normal admin
        //'level' => 3, --> for users

        Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'admin role for system-manger',
            'level' => 1,
        ]);
        Role::create([
            'name' => 'Research1',
            'slug' => User::Research1,
            'description' => 'For user creators',
            'level' => 3,
        ]);
        Role::create([
            'name' => 'Research2',
            'slug' => User::Research2,
            'description' => 'For user viewer',
            'level' => 3,
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
